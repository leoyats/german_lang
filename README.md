# *Zahlen* - Числа

```
0 - null      10 - zehn       20 - zwanzig
1 - eins      11 - elf        30 - dreißig
2 - zwei      12 - zwölf      40 - vierzig
3 - drei      13 - dreizehn   50 - fünfzig
4 - vier      14 - vierzehn   60 - sechzig
5 - fünf      15 - fünfzehn   70 - siebzig
6 - sechs     16 - sechzehn   80 - achtzig
7 - sieben    17 - siebzehn   90 - neunzig
8 - acht      18 - achtzehn   100 - hundert
9 - neun      19 - neunzehn   1000 - tausend
1000000 - Millionen
```

```
75 - fünf-und-siebzig
43 - drei-und-vierzig
66 - sechs-und-sechzig
```

```
749 - sieb-hundert-neun-und-vierzig
135 - hundert-fünf-und-dreißig
```

```
6432 - sechs-tausend-vier-hundert-zwei-und-dreißig
12432 - zwölf-tausend-vier-hundert-zwei-und-dreißig
212432 - zwei-hundert-zwölf-tausend-vier-hundert-zwei-und-dreißig
```

# *die Zeiten* - Часи

```
die Zeit (Zeiten) - час (часи)
die Sekunde (Sekunden) - секунда (секунди)
die Minute (Minuten) - хвилина (хвилини)
die Stunde (Stunden) - година (години)
der Tag (die Tage) - день (дні)
die Woche (Wochen) - тиждень (тижні)
der Monat (Monate) - місяць (місяці)
das Jahr (die Jahre) - рік (роки)
```

## *der Tag* - день

```
der (am) Morgen - ранок (зранку)
der (am) Abend - вечір (ввечері)
die (in der) Nacht - ніч (вночі)
```

```
der (am) Vormittag - (в період) до обіду
der (am) Mittag - (в) обід
der (am) Nachmittag - (в період) після обіду
```

## *die Tage* - дні
```
der (am) Montag - понеділок
der (am) Dienstag - вівторок
der (am) Mittwoch - середа
der (am) Donnerstag - четвер
der (am) Freitag - пятниця
der (am) Samstag - субота
der (am) Sonntag - неділя
```

## *die Monate* - місяці
```
der (im) Januar - (в) січень
der (im) Februar - (в) лютий
der (im) März - (в) березень
der (im) April - (в) квітень
der (im) Mai - (в) травень
der (im) Juni - (в) червень
der (im) Juli - (в) липень
der (im) August - (в) серпень
der (im) September - (в) вересень
der (im) Oktober - (в) жовтень
der (im) November - (в) листопад
der (im) Dezember - (в) грудень
```

## *Jehreszeiten* - пори року

```
der (im) Sommer - літо (влітку)
der (im) Herbst - осінь (восени)
der (im) Winter - зима (взимку)
der (im) Frühling - весна (весною)
```

# **Formen des Verbs** - Форми дієслова

## *Machen* - робити
```
ich mache     - я роблю
du machst     - ти робиш

er  ->        - він робить
sie -> macht  - вона робить
es  ->        - воно робить
ihr ->        - ви робите

wir ->        - ми робимо
sie -> machen - вони роблять
Sie ->        - Ви робите
```

## *Sein* - бути
```
ich bin     - я є
du bist     - ти є

er  ->      - він є
sie -> ist  - вона є
es  ->      - воно є

ihr seid    - ви є

wir ->      - ми є
sie -> sind - вони є
Sie ->      - Ви є
```


# *die Verbs* - Дієслова:

```
Der Indikativ - дійсний спосіб дієслова
Du schreibst eine Nachricht - Ти пишеш повідомлення.

Der Konjunktiv - умовний спосіб дієслова
Du schreibest eine Nachricht - Ти писав би повідомлення.

Der Imperativ - наказовий спосіб дієслова
Schreib eine Nachricht - Напиши повідомлення


Das Aktiv - активний стан дієслова
Maria liest ein Buch - Марія читає книгу

Das Passiv - пассивний стан дієслова
Das Buch wird gelesen - Книгу читають
```

**Infinitiv** (**Indikativ**/Теперішній: ich, du, er/sie/es, ihr)
[**Präteritum**/Простий минулий]
{**Perfekt**/Завершений теперішній}

## *modal Verbs* - Модальні дієслова

```
Dürfen (ich darf, du darfst, er darf, irh dürft, wir dürfen)[durfte]{dürfen} - мати дозвіл
Können (ich kann, du kannst, er kann, irh könnt, wir können)[könnte]{können} - могти
Mögen (ich mag, du magst, er mag, ihr mögt, wir mögen)[mochte]{mögen} - подобатись
Müssen (ich muss, du musst, er muss, ihr müsst, wir müssen)[müsste]{müssen} - я вирішив, що мені потрібно щось зробити
Sollen (ich soll, du sollst, ihr sollt, wir sollen)[sollte]{sollen} - мені сказали, що мені потрібно щось зробити
Wollen (ich will, du willst, er will, irh wollt, wir wollen)[wolte]{wollen} - хотіти
```

## *das starke Verb* - сильні дієслова

```
Backen (backe, bäckst, bäckt, ihr backt)[buk/backte]{gebacken} - пекти
Bitten (bitte, bittest, bittet)[bat]{gebeten} - просити
Einschlafen (schlafe ein, schläfst ein, schläft ein, ihr schlaft ein)[schlief ein]{eingeschlafen} - засипати
Fahren (fahre, fährst, fährt, ihr fahrt)[fuhr](bin){gefahren} - їхати
Fallen (falle, fällst, fällt, ihr fallt)[fiel](bin){gefallen} - падати
Finden (finde, findest, findet)[fand]{gefunden} - знаходити
Fliegen (fliege, fliegst, fliegt)[flog](bin){geflogen} - літати
Geben (gebe, gibst, gibt)[gab]{gegeben} - давати
Gehen (gehe,gehst,geht)[ging](bin){gegangen} - геен - йти
Helfen (helfe, hilfst, hilft)[half]{geholfen} - допомагати
Kommen (komme, kommst, kommt)[kam](bin){gekommen} - коммен - приходити
Lesen (lese, liest)[las]{gelesen} - читати
Liegen (liege, liegst, liegt)[lag]{gelegen} - лежати
Messen (messe, misst)[mass]{gemessen} - вимірювати
Schlafen (schlafe, schlafst, schlaft)[shlieft]{geschlafen} - спати
Schreiben (schreibe, schreibst, schreibt)[schreib]{geschreiben} - писати
Schwimmen (schwimme, schwimmst, schwimmt)[schwamm]{geschwommen} - плавати
Sehen (sehe, sehst, seht)[sah]{gesehen} - зеен - бачити
Singen (singe, singst, singt)[sang][gesungen] - співати
Sitzen (sitze, sitzt)[sass]{gesessen} - сидіти
Sprechen (sprehe, sprichst, spricht)[sprach]{gesprochen} - говорити
Springen (springe, springst, springt)[sprang]{gesprungen} - стрибати
Stehen (stehe, stehst, steht)[stand/stund](bin){gestanden} - стояти
Trinken (trinke, trinkest, trinkt)[trank]{getrunken} - пити
Tun (tu, tust, tut)[tat]{getan} - робити (рідко використовується)
Verstehen (verstehe, verstehst, versteht)[verstand]{verstanden} - розуміти
Wachsen (wachse, wächst, ihr wachst)[wuchs]{gewachsen} - рости
```

## *das unregelmäßiges Verb* - неправильні дієслова
```
Essen (esse, isst, irh esst)[ass]{gegessen} - їсти
Haben (ich habe, du hast, er hat, irh habt, wir haben)[hatte]{gehabt} - мати
Kennen (kenne, kennst, kennt)[kannte]{gekannt} - знати
Möchten (möchte, möchtest, möchtet)[möchte]{mögen} - хотілося б (ввічлива форма)
Nennen (nenne, nennst, nennt)[nannte]{genannt} - називати
Wissen (ich weiß, du weißt, er weiß, ihr wisst, wir wissen)[wusste]{gewusst} - знати
```

## *das schwache Verb* - слабкі дієслова
```
Antworten (antworte, antwortest, antwortet)[antwortete]{geantwortet} - відповідати
Arbeiten (arbeite, arbeitest, arbeitet)[arbeitete]{gearbeitet} - працювати
Atmen (atme, atmest, atmet)[atmete]{geatmet} - дихати
Baden (bade, badest, badet)[badete]{gebadet} - купатись
Bauen (baue, baust, baut)[baute]{gebaut} - будувати
Benutzen (benutze, benutzt)[benutzte]{benutzt} - бенуццен - використовувати
Boxen (boxe, boxt)[boxte]{geboxt} - боксувати
Brauchen (brauche, brauchst, braucht)[brauchte]{gebraucht} - потребувати/використовувати/застосовувати
Fragen (frage, fragst, fragt)[fragte]{gefragt} - питати
Heißen (heiße, heißt)[heiß]{geheißen} - називатись
Hören (höre, hörst, hört)[hörte]{gehört} - слухати
Hungern (hungere, hungerst, hungert)[hunderte]{gehungert} - голодувати
Kaufen (kaufe, kaufst, kauft)[kaufte]{gekauft} - купляти
Kochen (koche, kochst, kocht)[kochte]{gekocht} - готувати, кипятити
Kosten (koste, kostest, kostet)[kostete]{gekostet} - коштувати
Klingeln (klingle, klingelst, klingelt)[klinglte]{geklingelt} - дзвонити
Kranken (kranke, krankst, krankt)[krankte]{gekrankt} - страждати
Kränken (kränke, kränkst, kränkt)[kränkte]{gekränkt} - ображати
Kreuzen (kreuze, kreuzt)[kreuzte]{gekreuzt} - перехрещувати, схрещувати
Lachen (lache, lachst, lacht)[lachte]{gelacht} - сміятися
Lächeln (lächle, lächelst, lächelt)[lächlte]{gelächlt} - посміхатись
Leben (lebe, lebst, lebt)[lebte]{gelebt} - жити
Lieben (liebe, liebst, liebt)[liebte]{geliebt} - любити
Machen (mache, machst, macht)[machte]{gemacht} - робити
Malen (male, malst, malt)[malte]{gemalt} - малювати
Öffnen (öffne, öffnest, öffnet)[öffnete]{geöffnet} - відкривати
Passen (passe, passt)[passte]{gepasst} - підходити
Reden (rede, redest, redet)[redete]{geredet} - розмовляти
Reizen (reize, reizt)[reizte]{gereist} - мандрувати
Rücken (rücke, rückst, rückt)[rückte]{gerückt} - рухатись
Sacken (sacke, sackst, sackt)[sackte]{gesackt} - спускатись
Sagen (sage, sagst, sagt)[sagte]{gesagt} - казати
Sammeln (sammle, sammelst, sammelt)[sammelte]{gesammlt} - збирати
Schenken (schenke, schenkst, schenkt)[schenkte]{geschenkt} - дарувати
Schütteln (schütte, schüttelst, schüttelt)[schüttelte]{geschüttelt} - трясти
Spielen (spiele, spielst, spielt)[spielte]{gespielt} - грати
Suchen (suche, suchst, sucht)[suchte]{gesucht} - зухен - шукати
Tanzen (tanze, tanzt)[tanzte]{getanzt} - танцювати
Tippen (tippe, tippst, tippt)[tippte]{getippt} - друкувати
Trauern (trauere, trauerst, trauert)[trauerte]{getrauert} - оплакувати
Trocknen (trockne, trocknest, trocknet)[trocknete]{getrocknet} - сушити, сохнути
Üben (übe, übst, übt)[übte]{geübt} - тренуватись, займатись вправами
Wählen (wähle, wählst, wählt)[wählte]{gewählt} - вибирати, набирати, голосувати
Weißen (ich weiße, du weißt, er weiß, ihr weißt, wir weißen)[weißte]{geweißt} - білити
Wohnen (wohne, wohnst, wohnt)[wohnte]{gewohnt} - жити, проживати
Zahlen (zahle, zahlst, zahlt)[zahlte]{gezahlt} - платити
Zählen (zähle, zählst, zählt)[zählte]{gezählt} - рахувати
Zeigen (zeige, zeigst, zeigt)[zeigte]{gezeigt} - цайген - показувати
Zielen (ziele, zielst, zielt)[zielte]{gezielt} - цілитись, намічати
Zittern (zittere, zitterst, zittert)[zitterte]{gezittert} - тремтіти
```

# **die Nomen** - Іменники
## *Männlich* - Чоловічий рід
`Artikel Nomen (Artikel[opt] Mehrzahl) - іменник в однині (множині)`


```
der Ablauf (die Äblaufe) - послідовність, виконання, старт, сток, течія річки
der Anschlag (die Anschläge) - удар/замах/об'ява/афіша/кошторис/четверть
der Akzent (die Akzente) - ´
der Apfel (die Äpfel) - яблуко (яблука)
der (die) Artikel - артикль (артиклі)
der Ast (die Äste) - гілка (гілки)
der Ausgang (die Ausgänge) - вихід
der (die) Bäcker - пекар (пекарі)
der Ball (die Bälle) - мяч (мячі)
der Bus (die Busse) - автобус (автобуси)
der (die) Bügel - дуга, рамка
der Brauch (die Bräuche) - звичай (звичаї)
der Bruder (die Brüder) - брат (брати)
der Durst - спрага
der Eingang (die Eingänge) - вхід
der (die) Enkel - внук (внуки)
der Fakt (die Fakten/Fakts) - факт (факти)
der Fisch (die Fische) - риба (риби)
der Föhn (die Föhne) - фен (фени)
der Fuß (die Füße) - нога (ноги)
der Fußball - футбол
der Hals (die Hälse) - горло, шия (горла, шиї)
der Hund (die Hunde) - собака (собаки)
der Hunger - голод
der (die) Igel - їжак (їжаки)
der Juli - липень
der (die) Käse - сир (сири)
der (die) Kasus - відмінок (відмінки)
der (die) Kater - кіт (коти)
der (die) Kegel - кегля (кеглі)
der Koch (die Köche) - повар, повари
der Komponist (die Komponisten) - композитор (композитори)
der Kopf (die Köpfe) - голова (голови)
der (die) Körper - тіло (тіла)
der (die) Kuchen - пиріг (пироги)
der Kuß (die Küße) - поцілунок (поцілунки)
der (die) Lehrer - вчитель (вчителі)
der Lernerfolg - успіх у навчанні
der Löffel (die Löffel) - ложка (ложки)
der Löwe (die Löwen) - лев (леви)
der Luxus - розкіш
der (die) Maler - художник (художники)
der Mann (die Männer) - мужчина (мужчини)
der Mantel (die Mäntel) - пальто
der Mensch (die Menschen) - людина (люди)
der Müll - мусор
der Mund (die Münde) - рот (роти)
der (die) Muziker - музикант (музиканти)
der Name (die Namen) - ім'я, фамілія, прізвисько, назва, репутація
der Neffe (die Neffen) - племінник (племінники)
der (die) Onkel - дядько (дядьки)
der Opa (die Opas) - дідусь (дідусі)
der (die) Poster - плакат (плакати)
der Punkt (die Punkte) - точка (точки)
der (die) Rücken - спина (спини)
der Rucksack (die Rucksäcke) - рюкзак (рюкзаки)
der Sack (die Säcke) - мішок (мішки)
der Saft (die Säfte) - сік (соки)
der (die) Sänger - співак (співаки)
der Satz (die Sätze) - речення 
der (die) Schauspieler - актор (актори)
der See (die Seen) - озеро, море (озера, моря)
der Sekt (die Sekte) - шампанське
der Sohn (die Söhne) - син (сини)
der Stadtteil (die Stadtteile) - район (райони)
der Stuhl (die Stühle) - стілець (стільці)
der Tee (die Tees) - чай (чаї)
der Tisch (die Tische) - стіл (столи)
der Vati (die Vatis) - тато (тата)
der Wein (die Weine) - вино (вина)
der Wolf (die Wölfe) - вовк (вовки)
der Zoo (die Zoos) - цуу - зоопарк (зоопарки)
der Zahn (die Zähne) - зуб (зуби)
```

## *Weiblich* - Жіночий рід

```
die Ära (Ären) - ера (ери)
die Bäckerei (Bäckereien) - пекарня (пекарні)
die Banane (Bananen) - банан (банани)
die Begrüßung (Begrüßungen) - привітання (багато привітань)
die Bibel - бібел - Біблія
die Blume (Blumen) - квітка (квіти)
die Bombe (Bomben) - бомба (бомби)
die Buntwäsche - кольорова білизна
die Eltern - батьки
die Entschuldigung - вибачення
die Enkelin (Enkelinnen) - внучка (внучки)
die Familie (Familien) - сім'я (сім'ї)
die Farbe (Farben) - колір (кольори)
die Form (Formen) - форма (форми)
die Frage (Fragen) - питання (багато питань)
die Frau (Frauen) - жінка (жінки)
die Geschwister - брати-сестри
die Gesundheit (Gesundheiten) - будь здоров (будьте здорові)
die Grundform (Grundformen) - основна форма дієслова
die Hand (Hände) - рука (руки)
die Heimatstadt (Heimatstädte) - рідне місто (міста)
die Jacke (Jacken) - куртка (куртки)
die Kaltluft - холодне повітря
die Katze (Katzen) - кішка (кішки)
die Kleidung (die Kleidungen) - одяг
die Klingel (Klingeln) - дзвін (дзвони)
die Komponistin (Komponistennen) - композиторка (композитори)
die Konjunktion (Konjunktionen) - звязок (звязки)
die Kunst (die Künste) - мистецтво
die Lampe (Lampen) - лампа (лампи)
die Lehrerin (Lehrerinnen) - вчителька (вчительки)
die Liebe - кохання
die Linde (Linden) - липа (липи)
die Lücke (Lücken) - пробіл, прогалина
die Malerin (Malerinnen) - художниця (художниці)
die Milch - молоко
die Möwe (Möwen) - чайка (чайки)
die Mehrzahl (Mehrzahlen) - більшість, множина (множини)
die Mutter (Mütter) - мама (мами)
die Muzikerin (Muzikerinnen) - музикантка (музикантки)
die Nachricht (Nachrichten) - повідомлення
die Nase (Nases) - ніс (носи)
die Nichte (Nichten) - племінниця (племінниці)
die Oma (Omas) - бабця (бабці)
die Pastete (pasteten) - паштет (паштети)
die Pflanze (die Pflanzen) - рослина (рослини)
die Post - пошта
die Reihenfolge (Reihenfolge) - послідовність
die Reise (Reisen) - мандрівка (мандри)
die Sängerin (Sängerinnen) - співачка (співачки)
die Schauspielerin (Schauspielerinnen) - акторка (акторки)
die Schwester (Schwestern) - сестра (сестри)
die Seide (Seiden) - шовк
die Soße (Soßes) - зосе - соус
die Sonne (Sonnen) - сонце (сонця)
die Stadt (Städte) - місто (міста)
die Tante (Tanten) - тітка (тітки)
die Tilde (Tilden) - ~
die Tochter (Töchter) - дочка (дочки)
die Tür (Türen) - двері
die Verabschiedung (Verabschiedungen) - ствердження
die Wolke (Wolken) - хмара (хмари)
die Wurst (Würste) - ковбаса (ковбаси)
die Wut - злість
die Zahl (Zahlen) - число (числа)
```

## *Sächlich* - Середній рід

```
das Bett (die Betten) - ліжко (ліжка)
das Bier (die Biere) - пиво (багато пива)
das Brot (die Brote) - хліб (багато хліба)
das Buch (die Bücher) - книга (книги)
das Cafe (die Cafes) - кава (багато кави)
das Ding (die Dinge) - річ (речі)
das Dorf (die Dörfer) - село (села)
das Eis - морозиво
das Ende (die Enden) - кінець, кінці
das Foto (die Fotos) - фото
das Fräulein (die Dame, Damen) - панянка (панянки)
das Fragezeichen (die Fragezeichen) - знак (знаки) питання
das Gegenteil (die Gegenteile) - антонім (антоніми)
das Geschirr - посуд
das Glück (die Glücke) - щастя
das Gold - золото
das Grundzahlwort (die Grundzahlwörter) - кількісний числівник (числівники)
das Haar (die Haare) - волосок (волосся)
das Haus (zu Hause, viele Häuser) - будинок (вдома, багато будинків)
das Holz (die Hölzer) - деревина
das Kind (die Kinder) - дитина (діти)
das Kino (die Kinos) - кіно (кіна)
das Kreuz (die Keuze) - хрест, перехрестя
das Kreuzen - роспяття, обмін
das (die) Mädchen - дівчина (дівчата)
das Maß (die Maße) - міра (міри)
das (die) Messer - ніж (ножі)
das (die) Nomen - іменник (іменники)
das Ohr (die Ohren) - вухо (вуха)
das Öhr (die Öhren) - вушко (вушка)
das Öl (die Öle) - олія (олії)
das Passen - пригодність
das Pech (die Peche) - смола (смоли), невдача (невдачі)
das Possessivpronomen (die Possessivpronomen) - присвійний займенник (займенники)
das Pronomen (die Pronomina / Pronomen) - займенник (займенники)
das (die) Rätsel - казка (казки)
das Sacken - провисання
das Substantiv (die Substantive) - іменник (іменники)
das Schranktrocken - сушильний шкаф
das (die) Schriftsteller - письменник (письменники)
das Schwein (die Schweine) - свиня (свині)
das (die) Springen - стрибок (стрибки)
das Tier (die Tiere) - тварина (тварини)
das Trocknen - висихання, обезвоження
das Verb (die Verben) - дієслово
das Vieh - худоба
das Volk (die Völker) - народ (народи)
das Wählen - вибір
das Wasser (die Wasser / Wässer) - вода (води)
das Wort (die Wörten) - слово (слова)
das Zahlwort (die Zahlwörter) - числівник (числівники)
das Ziel (die Ziele) - ціль (цілі)
das (die) Zimmer - кімната (кімнати)
```

# *das Personalpronomen* - Форми іменника (субьект, обьект)

```
_____________________________________________________
Wer? (Хто?)|Wen? (Кого?) |Wem? (Кому?) |Wessen?(Чій) |
Nominativ  |Akkusativ    |Dativ        |Genitiv      |
---------------------------------------|-------------|
ich (я)    | mich (мене) | mir (мені)  |meiner (мій) |
du (ти)    | dich (тебе) | dir (тобі)  |deiner (твій)|
er (він)   | ihn (його)  | ihm (йому)  |seiner (його)|
sie (вона) | sie (її)    | ihr (їй)    |ihrer (її)   |
es (воно)  | es (його)   | ihm (йому)  |seiner (його)|
ihr (ви)   | euch (вас)  | euch (вам)  |euer (ваш)   |
wir (ми)   | uns (нас)   | uns (нам)   |unser (наш)  |
sie (вони) | sie (їх)    | ihnen (їм)  |ihrer (їх)   |
Sie (Ви)   | Sie (Вас)   | Ihnen (Вам) |Ihrer (Ваш)  |
_______________________________________|_____________|
```

## **die Kasus** - Відмінки
```
________________________________|___Чол.р._Одн.__|_Жін.р_Одн._|____Сер.р_Одн.___|_Множина_|
Nominativ(Wer?/Was? - Хто?/Що?) | der/ein        | die/eine   | das/ein         | die/ -  |
Genitiv(Wessen? - Чий?)         | des(s)/eines(s)| der/einer  | des(s)/eines(s) | der/ -  |
Dativ(Wem? - Кому?)             | dem/einem      | der/einer  | dem/einem       | den/ -  |
Akkusativ(Wen?/Was? - Кого?/Що?)| den/einen      | die/eine   | das/ein         | die/ -  |
------------------------------------------------------------------------------------------|
```


## **die Pronomen** - Займенники

### **die Possessivpronomen** Присвійні займенники
`Це твій дім? - Das ist dein Haus?`

```
__________________________________________________
Ім. |__________Однина_________________|  Множина  |
    |  Чол.р.  |   Жін.р   |  Сер.р.  |           |
----|----------|-----------|----------|-----------|
ich |mein(мій) |meine(моя) |mein(мій) |meine(мої) |
du  |dein(твій)|deine(твоя)|dein(твій)|deine(твої)|
er  |sein(його)|seine(його)|sein(його)|seine(його)|
sie |ihr(її)   |ihre(її)   |ihr(її)   |ihre(її)   |
es  |sein(його)|seine(його)|sein(його)|seine(його)|
ihr |euer(ваш) |eure(ваша) |euer(ваш) |eure(ваші) |
wir |unser(наш)|unsre(наша)|unser(наш)|unsre(наші)|
sie |ihr(їх)   |ihre(їх)   |ihr(їх)   |ihre(їх)   |
Sie |Ihr(Ваш)  |Ihre(Ваша) |Ihr(Ваше) |Ihre(Ваші) |
--------------------------------------------------|
```

### Відповідь на питання

`Так, мій - Ja, das ist meiner`
```
________________________________________________________
Ім. |__________________Однина_______________|  Множина  |
    |   Чол.р.   |   Жін.р   |    Сер.р.    |           |
----|------------|-----------|--------------|-----------|
ich |meiner(мій) |meine(моя) |mein(e)s(мій) |meine(мої) |
du  |deiner(твій)|deine(твоя)|dein(e)s(твій)|deine(твої)|
er  |seiner(його)|seine(його)|sein(e)s(його)|seine(його)|
sie |ihrer(її)   |ihre(її)   |ihr(e)s(її)   |ihre(її)   |
es  |seiner(його)|seine(його)|sein(e)s(його)|seine(його)|
ihr |euerer(ваш) |eure(ваша) |euer(e)s(ваш) |eure(ваші) |
wir |unserer(наш)|unsre(наша)|unser(e)s(наш)|unsre(наші)|
sie |ihrer(їх)   |ihre(їх)   |ihr(e)s(їх)   |ihre(їх)   |
Sie |Ihrer(Ваш)  |Ihre(Ваша) |Ihr(e)s(Ваше) |Ihre(Ваші) |
--------------------------------------------------------|
```

## Кольори (Farben)
```
Blau - синій
Gelb - жовтий
Grau - сірий
Grün - зелений
Lila - фіолетовий
Orange - оранжевий
Rot - червоний
Schwarz - чорний
Weiß - білий
```

# **Konjunktionen** -  Додаткові слова

```
alles - все
circa - цирка - майже
diese - це, цей, ці
etwas - щось
genau - точно
genug - достатньо
hier - тут
jede - кожен
nichts - нічого
öde - скучно
pflegeleicht - не потребує догляду
sehr - дуже
sicher - надійно
weiter - більше, понад, далі
```
```
gestern - гестерн - вчора
heute - хьойте - сьогодні
jetzt - єтцт - зараз
morgen - морген - завтра
```
```
alle - всі
jemand - хтось
niemand - ніхто
```
```
immer - завжди
manchmal - інколи
nie - ніколи
```

# Прикметники та прислівники прості та неправильні

```
______Positiv______|_______Komparativ______|__________Superlativ___________|
bald(скоро)        |eher(раніше)           |am ehersten(раніше всього)     |
hoch(високий)      |höcher(вищий)          |höchst(найвищий)               |
gern(охоче)        |lieber(охочіше)        |am liebsten(найохочіше)        |
groß(великий)      |größer(більший)        |am größten(найбільший)         |
gut(хороший)       |besser(кращий)         |best(найкращий)                |
nah(близький)      |näher(ближчий)         |nächst(найближчий)             |
viel(багато)       |mehr(більше)           |am meisten(більше всього)      |
---------------------------------------------------------------------------|
ähnlich(схожий)    |ähnlicher(схожий)      |am ähnlichsten(найсхожіший)    |
alt(старий)        |älter(старіший)        |am ältesten(найстаріший)       |
böse(злий)         |böser(зліший)          |am bösesten(найзліший)         |
dumm(дурний)       |dümmer(дурніший)       |am dümmsten(найдурніший)       |
durstig(спраглий)  |durstiger(спрагліший)  |am durstigsten(найспраглий)    |
feucht(вологий)    |feuchter(вологіший)    |am feuchtesten(найвологіший)   |
glücklich(щасливий)|glücklicher(щасливіший)|am glücklichsten(найщасливіший)|
groß(великий)      |größer(більший)        |am größten(найвеличніший)      |
hungrig(голодний)  |hungriger(голодніший)  |am hungrigsten(найголодніший)  |
klein(маленький)   |kleiner(меньший)       |am kleinsten(найменший)        |
krank(хворий)      |kränker(хворіший)      |am kränksten(найхворіший)      |
kurz(короткий)     |kürzer(коротший)       |am kürzesten(найкоротший)      |
lang(довгий)       |länger(довший)         |am längsten(найдовший)         |
müde(втомленний)   |müder(втомленіший)     |am müdesten(найвтомленіший)    |
nett(симпатичний)  |netter(симпатичніший)  |am nettesten(найсимпатичніший) |
neu(новий)         |neuer(новіший)         |am neuesten(найновіший)        |
schwach(слабкий)   |schwächer(слабкіший)   |am schwächsten(найслабший)     |
sicher(надійний)   |sicherer(надійніший)   |am sichersten(найнадійний)     |
stark(сильний)     |stärker(сильніший)     |am stärksten(найсильніший)     |
süß(солодко)       |süßer(солодше)         |am süßesten(найсолодше)        |
trocken(сухо)      |trockener(сухіше)      |am trockensten(найсухіше)      |
wichtig(важливий)  |wichtiger(важливіший)  |am wichtigsten(найважливіший)  |
```


# **die Begrüßungen** - Привітання:

```
Guten Morgen, Tag, Abend - доброго ранку, обіду, вечера
Gute Nacht - доброї ночі
Auf Wiedersehen - до побачення
Auf Wiederhören - почуємось
Danke (schön) - (велике) спасибі
Bitte (schön) - (велике) будь ласка
Hallo - привіт
Tschüss - бувай
Wir, bitte - що-що?
Sagen sie mir bitte - Скажіть будь ласка
Wie geht es dir? - Як у тебе справи
Wie geht es ihnen? - Як у Вас справи
(sehr) gut - (дуже) добре
(nicht) schlecht - (не) погано
(sehr) angenehm - (дуже) приємно
```

# Особливі випадки:

```
ei - ай
eu - ой
ch - ч
sch - ш
tsch - ч
Deutsch - дойч
```

```
Ö - еео
Ü - ееу
Ä - ееe
```

# **Konjunktionen** - Звязки:

```
aber - але
also - ітак
auch - теж
aus - з, від
bis - до
da - там, тут
ja - так
man - дехто, кажуть
na ja - ну
nein - ні
oder - чи
sondern - зондерн - а ->
nicht du, sondern sie - не ти, а вона
und - і
von - від
zum - в, у, до, на, для
```

## Напрямок
```
aus - з (вінниці)
in - в (кельні) (десь)
nach - в (кельн) (куди)
```


## Різні форми в реченні:

```
Ich arbeite mit dieser Frau. - Я працюю з цією жінкою <- Präsens/Теперішній час
Ich arbeitete gegen eine Regierung. - Я працювала для уряду <- Vergangenheitsform Präteritum/Минулий час
Ich werde dort am Freitag arbeiten. - Я там працюватиму у п'ятницю. <- Futur I/Майбутній час
Ich habe gelesen eine Buch - я прочитав книгу <- Perfect/Теперішній
Ich hatte gestern gearbeitet - Я прочитав книгу вчора <- PlusquamPerfekt/Минулий (до якогось моменту)
Ich mache nicht - я не роблю <- Негативна форма

Mache ich - Роблю я? <- Питальна форма
Ich habe nicht ge+mach+t - я не зробив <- Негативна Минула
Habe ich gemacht - я зробив? <- Минула Питальна
Ich bin ge+gangen - я ходив
Ich bin ge+kommen - я прийшов
```

# Питання:

## **Was - Що?**

```
Was hast du gemacht - Що ти зробив?
```

## **Wer - Хто?**

```
Wer hat gesagt? - Хто сказав?
Wer bist du? - Хто ти?
```

## **Wo - Де?**

```
Wo wohnst du? - Де ти живешь?
Wo hast du gearbeitet? - Де ти працював?
Wo bist du? - Де ти?
Wo sind wer? - Де ми?
```

## **Wann - Коли?**

```
Wann arbeitest du? - Коли будешь працювати?
Wann hast du gesingt? - Коли ти співав?
```

## **Warum - Чому?**

```
Warum arbeitest du nicht? - Чому ти не працюєш?
Warum hat er geantwortet nicht? - Чому він не відповідає?
```

## **Wie? - Як?**

```
Wie machst du? - Як ти робиш?
Wie hat sie getanzt? - Як вона танцювала?
Wie alt bist du? - Скільки тобі років? (Як старий ти є)
Wie lange? - Як довго?
Wie oft? - Як часто?
Wie weit? - Як далеко?
```

## **Wie viel? - Скільки?**

```
Wie viel kostet das? - Скільки це коштує?
```


## **Wen? - Кого?**

```
Wen liebst du? - Кого ти любиш?
Wen hast du geliebt? - Кого ти любила?
```

## **Wohin? - Куди?**

```
Wohin gehst du? Куди ти йдеш?
```

## **Woher? - Звідки?**

```
Woher bist gekommen du? Звідки ти прийшов?
```

# Речення:

```
Wer heißt du? - Хто ти?
Ich heiße Vitalii - Меня звати Віталій
Ich wohne in Köln - Я живу в Кельні
Du fragst mich, ihr antworte dir <- Ти мене питаєш, я відповідаю тобі
Was hast du ihr gesagt? <- Що ти їй сказав?
Ich habe ihr alles gesagt <- Я їй все сказав
Ich sage euch alles <- Я вам все скажу
Sag mir schnell, ja oder nein? <-  Скажи мені швидко, так чи ні?
Woher kommst du? <- Звідки ти родом?
Ihr komme aus Ukraine <- Я з України
Hier kann man gut essen <- Тут можна добре поїсти
Was kann man hier kochen? <- Що тут можна приготувати?
Darf ich herein? <- Можна мені увійти?
Darf ich hereus? <- Можна мені вийти?
Darf ich durch? <- Можна мені пройти?
Du dafst das nicht machen <- Тобі не можна це робити
Wo willst du heute schlafen? <- Де ти сьогодні хочеш спати?
Ich mag Tee <- Я люблю чай
Was magst du? <- Що тобі подобається?
Ich möchte etwas fragen - Мені хотілося б щось спитати
Ich brauche den(einen) Computer - Мені потрібно цей(будь-який) компютер
Ich brauche das(ein) Auto - Мені потрібне це(будь-яке) авто
```

```
Ich muss im Bett liegen - Я повинен лежати в ліжку (я так вирішив)
Ich soll im Bett liegen - Я повинен лежати в ліжку (лікар так вирішив)
```

# Для чоловічого роду *der -> keinen*

```
Ich mag keinen Tee <- Я не люблю чай
Ich mag kainen Fisch <- Я не люблю рибу
```

# Для жіночого роду *die -> keine*

```
Du magst keine Milch <- Ти не любиш молока
Du magst keine Wurst <- Ти не любиш молока
```

# Для середнього роду das -> kein

```
Ich mag kein Bier <- Я не люблю пиво
```

# Звуки

```
A a |  [aː]       |  Anton     |  /a/, /a:/      |  Band, Tag           |  Після i и j чується як ja — Maria;
                                                                           ai — дифтонг /aɪ̯/ («ай»); ae — чується ä;
                                                                           au — дифтонг /aʊ̯/; подвоєне aa може
                                                                           читатись довго — Haar («волосся»).
_____________________________________________________________________________________________________________________
Ä ä |  [ɛː]       |  Ärger     |  /ε/, /ε:/      |  Hände, Ähre         |  Перед u — дифтонг /ɔʏ̯/ (оу).
B b |  [beː]      |  Berta     |  /b/            |  Bruder              |
C c |  [tseː]     |  Cäsar     |                 |  Creme               |  Зустрічається в поєднанні з іншими буквами:
                                                                           с h створює буквосполучення ch, чуєме як
                                                                           /x/ или /ç/; ck — /k/; в chs — /ks/; tsch — /t͡ʃ/;
                                                                           рідко одна c чується як /t͡s/ (перед e, i);
                                                                           на початку слів читается як /k/.
____________________________________________________________________________________________________________________________
D d |  [deː]      |  Dora      |  /d/            |  ausdenken           |
E e |  [eː]       |  Emil      |  /ε/, /ə/, /e:/ |  kennen, bekannt, See|  ie — /i:/, oe — /ø:/; ei, ey, ai, ay — дифтонг /aɪ̯/.
F f |  [ɛf]       |  Friedrich |  /f/            |  Fracht              |  Такий же звук дає німецка буква v.
G g |  [geː]      |  Gustav    |  /g/, /ʒ/, /ç/  |  gut, Genie, König   |
H h |  [haː]      |  Heinrich  |  /h/            |  heute               |  Часто h взагалі не читается, наприклад, між голосними
                                                                           і в кінці слова — gehen (розділяє дві e, і в цьому
                                                                           слові вони читаются роздільно, а не як один
                                                                           довгий звук; «ходити»), weh («хворий»).
______________________________________________________________________________________________________________________________
I i |  [iː]       |  Ida       |  /ı/, /i:/      |  bitten, Vieh        |  В ie — /i:/; ei, ai — /aɪ̯/.
J j |  [jɔt]      |  Jakob     |  /j/, /ʒ/       |  jung, Journalist    |
K k |  [kaː]      |  Konrad    |  /k/            |  Kette               |  В буквосполученні ck — /k/.
L l |  [ɛl]       |  Ludwig    |  /l/            |  Flöte               |
M m |  [ɛm]       |  Martha    |  /m/            |  Stimme              |
N n |  [ɛn]       |  Nordpol   |  /n/            |  senden              |
O o |  [oː]       |  Otto      |  /ɔ/, /o:/      |  offen, Kohl         |  В буквосполученні oe — /ø:/.
Ö ö |  [øː]       |  Ökonom    |  /œ/, /ø:/      |  Österreich, zwölf   |
P p |  [peː]      |  Paula     |  /p/            |  Punkt               |  pf — аффриката /p͡f/.
Q q |  [kuː]      |  Quelle    |                 |  Quadrat             |  В сполученні qu — /kv/.
R r |  [ɛr]       |  Richard   |  /r/            |  Drache              |
S s |  [ɛs]       |  Samuel    |  /s/, /z/       |  Bus, sehen          |  В sp и st — /ʃ/; sch — /ʃ/.
ẞ ß |  [ɛs’t͡sɛt]  |  Eszett    |  /s/            |  heiß                |  Стоїть в середині чи в кінці слова і читается
                                                                           як звичайний звук /s/.
______________________________________________________________________________________________________________________
T t |  [teː]      |  Theodor   |  /t/            |  Platte              |
U u |  [uː]       |  Ulrich    |  /ʊ/, /u:/      |  unten, Uhr          |  au — /aʊ̯/, eu, äu — /ɔʏ̯/.
Ü ü |  [yː]       |  Übermut   |  /ʏ/, /y:/      |  Übung, küssen, kühl |
V v |  [faʊ]      |  Viktor    |  /f/, /v/       |  Vater, Vase         |
W w |  [veː]      |  Wilhelm   |  /v/            |  Wolken              |
X x |  [iks]      |  Xanthippe |                 |  Xylophon            |  Дає звукосполучення /ks/ — Max.
Y y |  ['ʏpsilɔn] |  Ypsilon   |  /ʏ/, /y:/      |  Ypsilon, Typ        |
Z z |  [t͡sɛt]     |  Zacharias |  /t͡s/           |  dreizehn            |

```

# New cover letter

Hallo,

mein Name ist Vitalii Drevenchuk.
Ich suche eine neue Wohnung für meine Frau, meinen 2 Jahre alten Sohn und mich.
Seit einem Monat arbeite ich als Softwareentwickler bei der Cognotekt GmbH in Rodenkirchen, komme aus der Ukraine und spreche ein bisschen Deutsch.

Freundliche Grüße

Vitalii Drevenchuk
